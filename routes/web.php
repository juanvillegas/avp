<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return redirect('/blog'); });

Auth::routes();

Route::get('/users', 'UserController@index')->name('userIndex');
Route::get('/api/users', 'Api\UsersController@index')->name('apiUserIndex');

Route::get('/blog', 'BlogController@index')->name('blogIndex');
Route::get('/blog/create', 'BlogController@create')->name('createNewBlogEntry');
Route::post('/blog/create', 'BlogController@doCreate')->name('blogEntryDoCreate');
Route::get('/blog/{blogEntryId}', 'BlogController@single')->name('blogSingle');
