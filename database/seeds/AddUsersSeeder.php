<?php

use App\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class AddUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$factory = Factory::create();

    	User::truncate();

        for ($i = 0; $i < 10; $i++) {
	        User::create([
		        'first_name' => $factory->firstName(),
		        'last_name' => $factory->lastName(),
		        'password' => \Hash::make('123123123'),
		        'email' => $factory->email(),
		        'created_at' => $factory->dateTime()
	        ]);
        }

	    User::create([
		    'first_name' => 'demo',
		    'last_name' => 'demo',
		    'password' => \Hash::make('123123123'),
		    'email' => 'demo@demo.com',
		    'created_at' => $factory->dateTime()
	    ]);
    }
}
