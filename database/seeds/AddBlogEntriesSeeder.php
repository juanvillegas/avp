<?php

use App\BlogEntry;
use App\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class AddBlogEntriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$factory = Factory::create();

    	BlogEntry::truncate();

        for ($i = 0; $i < 10; $i++) {
	        BlogEntry::create([
		        'title' => $factory->sentence(6, true),
		        'content' => $factory->text(1000),
		        'slug' => $factory->slug(),
		        'user_id' => User::first()->id
	        ]);
        }

    }
}
