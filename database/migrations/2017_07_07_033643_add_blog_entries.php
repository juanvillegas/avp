<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBlogEntries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_entries', function($table) {
	        $table->increments('id');
	        $table->string('title');
	        $table->string('slug');
	        $table->text('content');
	        $table->unsignedInteger('user_id');
	        $table->timestamps();

	        $table->index('user_id');
	        $table->unique('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blog_entries');
    }
}
