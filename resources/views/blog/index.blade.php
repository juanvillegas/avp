@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Blog</div>

                <div class="panel-body">
                    @foreach($entries as $blogEntry)
                        <div class="blog-post">
                            <h2><a href="{{ route('blogSingle', ['blogEntryId' => $blogEntry->id]) }}">{{ $blogEntry->title }}</a></h2>
                            <div>
                                {!! substr($blogEntry->content, 0, 500) !!} ...
                                <a href="{{ route('blogSingle', ['blogEntryId' => $blogEntry->id]) }}">[view full post]</a>
                            </div>

                            <hr>

                            @if (Auth::user())
                                <p><i>published [localized]: {{ $blogEntry->displayLocalizedDate(Auth::user()) }}</i></p>
                            @endif
                            <p><i>published [utc]: {{ $blogEntry->created_at }}</i></p>
                        </div>
                    @endforeach

                    @if (Auth::user())
                        <div class="toolbar">
                            <a href="{{ route('createNewBlogEntry') }}" class="btn btn-md btn-primary">Create new</a>
                        </div>
                    @endif
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
