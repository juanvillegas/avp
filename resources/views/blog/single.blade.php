@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1>{{ $blogEntry->title }}</h1>
                    </div>

                    <div class="panel-body">
                        <div class="blog-post">
                            <div class="blog-post-meta">
                                @if (Auth::user())
                                    <p><i>published [localized]: {{ $blogEntry->displayLocalizedDate(Auth::user()) }}</i></p>
                                @endif
                                <p><i>published [utc]: {{ $blogEntry->created_at }}</i></p>
                            </div>

                            <hr>

                            <div>{!! $blogEntry->content !!}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
