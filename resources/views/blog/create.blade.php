@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create blog entry</div>

                <div class="panel-body">
                    <blog-entry-create inline-template>
                        <div>
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    There are errors in the form!
                                </div>
                            @endif

                            <form method="POST" action="{{ route('blogEntryDoCreate') }}" class="form" v-on:submit="onSubmit">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="">Title</label>
                                    <input type="text" class="form-control" name="title" value="{{ old('title') }}">

                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="">Slug</label>
                                    <input type="text" class="form-control" name="slug" value="{{ old('slug') }}">

                                    @if ($errors->has('slug'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('slug') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="">Content</label>
                                    <textarea name="content" class="summernote">{{ old('content') }}</textarea>

                                    @if ($errors->has('content'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('content') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <button type="submit" class="btn btn-md btn-primary">Create</button>
                            </form>
                        </div>
                    </blog-entry-create>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
