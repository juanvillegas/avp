const _ = require("lodash");
/**
 * This shim is included to allow Axios (and the Promise object in general) to support .finally() syntax.
 */
const promiseFinally = require("promise.prototype.finally");
promiseFinally.shim();

require("./bootstrap");

window.Vue = require('vue');

Vue.component("user-list", require("./components/UserList.vue"));
Vue.component("user-editor", require("./components/UserEditor.vue"));
Vue.component("user-row", require("./components/User.vue"));
Vue.component("user-create-modal", require("./components/UserCreateModal.vue"));
/**
 * This component allows transforming any textareas with the class "summernote" within its scope into a Summernote
 * editor. It will detect if the server is passing old() data (from previous submission) and initialize the summernote editor
 * with it. It will also hook into the containing form's submit() event and set the textarea's content to whatever is
 * inside the Summernote WYSIWYG
 */
Vue.component("blog-entry-create", require("./components/BlogEntryCreate.vue"));

const app = new Vue({
    el: "#app"
});
