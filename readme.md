# Install

- composer install
- npm install
- npm run dev
- php artisan migrate
- php artisan db:seed

## Sample log in:
- user: demo@demo.com
- pass: 123123123