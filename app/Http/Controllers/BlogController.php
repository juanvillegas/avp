<?php

namespace App\Http\Controllers;

use \Auth;
use App\BlogEntry;
use Illuminate\Http\Request;

class BlogController extends Controller
{

	public function index()
	{
		$entries = BlogEntry::orderBy('created_at', 'desc')->get();

		return view('blog.index', ['entries' => $entries]);
	}

	public function single($blogEntryId)
	{
		$blogEntry = BlogEntry::find($blogEntryId);

		return view('blog.single', ['blogEntry' => $blogEntry]);
	}

	public function create() {
		$this->middleware('auth');

		return view('blog.create');
	}

	public function doCreate(Request $request) {
		$this->middleware('auth');

		$this->validate($request, [
			'title' => 'required',
			'content' => 'required',
			'slug' => 'required|slug|unique:blog_entries,slug'
		]);

		$data = $request->all();
		$data['user_id'] = Auth::id();

		BlogEntry::create($data);

		return redirect('blog');
	}

}
