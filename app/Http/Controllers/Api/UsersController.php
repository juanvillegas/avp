<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;


class UsersController extends Controller
{

	public function index() {
		return User::all();
	}

	public function delete($userId) {
		User::destroy($userId);

		return response()->json();
	}

	public function doCreate(Request $request) {
		$this->validate($request, [
			'first_name' => 'required|string|min:2|max:255',
			'last_name' => 'required|string|min:2|max:255',
			'password' => 'required|string|min:6|max:255',
			'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email']
		]);

		$data = $request->only(['first_name', 'last_name', 'email']);
		$data['password'] = \Hash::make($request->get('password'));
		$user = User::create($data);

		return $user;
	}

	public function update(Request $request, $userId) {
		$this->validate($request, [
			'first_name' => 'required|string|min:2|max:255',
			'last_name' => 'required|string|min:2|max:255',
			'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($userId)]
		]);

		$user = User::find($userId);

		$user->fill($request->only(['first_name', 'last_name', 'email']));
		$user->save();
	}
	
}
