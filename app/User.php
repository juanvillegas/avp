<?php

namespace App;

use Auth;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'timezone_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getNameAttribute()
    {
    	return $this->first_name . ' ' . $this->last_name;
    }

    public function getCreatedAtAttribute($value)
    {
        $retVal = new Carbon($value, 'UTC');

        if (Auth::user()) {
	        $retVal->setTimezone(Auth::user()->timezone->zone_name);
        }

        return $retVal->toDateTimeString();
    }

    public function timezone()
    {
    	return $this->belongsTo('App\Timezone');
    }
}
