<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timezone extends Model
{

	public $timestamps = false;
	protected $fillable = ['country_code', 'zone_name'];

}
