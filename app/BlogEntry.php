<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class BlogEntry extends Model
{

	protected $fillable = ['title', 'slug', 'content', 'user_id'];

	public function displayLocalizedDate($user)
	{
		$date = new Carbon($this->created_at, 'UTC');
		$date->setTimezone($user->timezone->zone_name);
		return $date;
	}

}
